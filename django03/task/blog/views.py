from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    return render(request, "index.html")

def posts(request):
    return render(request, "posts.html")
    #return HttpResponse("List of posts")

def post(request,post_id):
    n = int(post_id)
    friends = []
    for i in range(n):
        friends.append(i)
    return render(request, "post.html", {"friends" : friends})


def post_new(request):
    return render(request, "add.html")

def post_edit(request,post_id):
    return render(request, "edit-delete.html", {"id": post_id, 'type': "Editing"})

def post_delete(request,post_id):
    return render(request, "edit-delete.html", {"id": post_id, 'type': "Deleting"})