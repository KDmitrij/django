from django.conf.urls import patterns, include, url

urlpatterns = patterns('blog.views',
    url(r'^$', 'index', name='index'),
    url(r'^posts$', 'posts', name="posts"),
    url(r'^posts/(?P<post_title>\w+)$', 'post', name="post"),
    url(r'^posts/post/new$', 'post_new', name="post_new"),
    url(r'^posts/(?P<post_title>\w+)/edit$', 'post_edit', name="post_edit"),
    url(r'^posts/(?P<post_title>\w+)/delete$', 'post_delete', name="post_delete"),
)
