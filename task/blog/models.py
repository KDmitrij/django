from django.db import models


class Post(models.Model):
    post_title = models.CharField(max_length=15)
    post_text = models.CharField(max_length=500)
    post_date = models.DateTimeField()

    def __unicode__(self):
        return self.post_title


class Comment(models.Model):
    comment_text = models.CharField(max_length=100)
    comment_Date = models.DateTimeField()
    post = models.ForeignKey(Post)

    def __unicode__(self):
        return self.comment_text
    list_display = ('comment_Date', 'comment_text', 'post')
