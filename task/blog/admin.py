from django.contrib import admin
from blog.models import Post, Comment


class CommentAdmin(admin.ModelAdmin):
    list_display = ('comment_Date', 'comment_text', 'post')


class PostAdmin(admin.ModelAdmin):
    list_display = ('post_title', 'post_text', 'post_date')
admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)