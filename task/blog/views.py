from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from blog.models import Post


def index(request):
    return render(request, "index.html")


def posts(request):
    posts = Post.objects.all()
    return render(request, "posts.html", {"posts": posts})


def post(request, post_title):
    post = Post.objects.get(post_title = post_title)
    return render(request, "post.html", {"post": post})


def post_new(request):
    if request.POST:
        s = Post()
        s.post_title = request.POST["post_title"]
        s.post_date = request.POST["post_date"]
        s.post_text = request.POST["post_text"]
        s.save()
        return HttpResponseRedirect(
            reverse(post, args=(s.post_title,))
        )
    else:
        return render(request,
                      "new.html")


def post_edit(request, post_title):
    if request.POST:
        s = Post()
        s.post_title = request.POST["post_title"]
        s.post_date = request.POST["post_date"]
        s.post_text = request.POST["post_text"]
        Post.objects.update(post_title=post_title, )
        return HttpResponseRedirect(
             reverse(post, args=(s.post_title,))
        )
    else:
        return render(request, "edit.html", {"post": Post.objects.get(post_title=post_title)})


def post_delete(request, post_title):
    if request.POST:
        s = Post.objects.get(post_title=post_title)
        s.delete()
        return HttpResponseRedirect(
            reverse(posts)
        )
    else:
        return render(request, "delete.html", {"post_title": post_title})