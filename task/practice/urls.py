from django.conf.urls import patterns, url

urlpatterns = patterns('practice.views',
    url(r'^$', 'index'),
    url(r'^parse$', 'parse'),
)
