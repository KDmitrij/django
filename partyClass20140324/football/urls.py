
from django.conf.urls import url, patterns


urlpatterns = patterns('football.views',
    url(r'^math$', 'matches', name='matches'),
    '''url(r'math/(?P<post_id>\d+)$', 'math', name='math'),
    url(r'team$', 'teams', name='teams'),
    url(r'team/(?P<post_id>\w+)$', 'team', name='team'),
    url(r'/$', 'registration', name='registration'),
    url(r'team/(?P<post_id>\w+)/delete$', 'delete_team', name='delete_team'),
    url(r'team/(?P<post_id>\w+)/add$', 'add_team', name='add_team'),
    url(r'match/(?P<post_id>\w+)/delete$', 'delete_team', name='delete_team'),
    url(r'match/(?P<post_id>\w+)/add$', 'add_team', name='add_team'),'''

)
