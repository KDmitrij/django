from django.db import models


class Team(models.Model):
    name = models.CharField(max_length=20)
    count = models.IntegerField()
    points = models.IntegerField()
    goal = models.IntegerField()
    loss = models.IntegerField()
    win = models.IntegerField
    lost = models.IntegerField()
    draw = models.IntegerField()


class Match(models.Model):
    first_team = models.ForeignKey(Team)
    second_team = models.ForeignKey(Team)
    score = models.CharField(max_length=3)
    date =  models.DateField()