from django.conf.urls import patterns, url

urlpatterns = patterns('myMath.views',
    url(r'^factorial/(?P<key>\d+)$', 'factorial'),
    url(r'^factorization/(?P<k>\d+)$', 'factorization')
)
