from django.http import HttpResponse


def getfactorial(key):
    if key < 2:
        return 1
    return getfactorial(key - 1) * key


def factorial(request, key):
    return HttpResponse(getfactorial(int(key)))


def getfactorization(k):
    if k < 2:
        return ''
    i = 2
    while k % i != 0:
        i += 1
    return str(i) + ' ' + str(getfactorization(k/i))


def factorization(request, k):
    return HttpResponse(getfactorization(int(k)))
