from django.http import HttpResponse


def index(request):
    return HttpResponse('Sorry, no books')

def book(request, key):
    return HttpResponse('Book with number ' + key + " is not exist")

def page(request, key,page):
    return HttpResponse("Page " + page + " book`s  " + key + " is not exist.")