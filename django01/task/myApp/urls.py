from django.conf.urls import patterns, url

urlpatterns = patterns('myApp.views',
                       url(r'^$', 'index'),
                       url(r'^(?P<key>\d+)$', 'book'),
                       url(r'^(?P<key>\d+)/(?P<page>\d+)$', 'page')
    )
