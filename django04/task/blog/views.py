from django.http import HttpResponse
from django.shortcuts import render
from blog.models import Post


def index(request):
    return render(request, "index.html")


def posts(request):
    posts = Post.objects.all()
    return render(request, "posts.html", {"posts": posts})
    #return HttpResponse("List of posts")


def post(request, post_title):
    post = Post.objects.get(post_title = post_title)
    return render(request, "post.html", {"post": post})


def post_new(request):
    return render(request, "add.html")


def post_edit(request, post_title):
    return render(request, "edit-delete.html", {"id": post_title, 'type': "Editing"})


def post_delete(request, post_title):
    return render(request, "edit-delete.html", {"id": post_title, 'type': "Deleting"})