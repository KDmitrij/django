from django.conf.urls import patterns, include, url

urlpatterns = patterns('blog.views',
    url(r'^$', 'index'),
    url(r'^posts$', 'posts'),
    url(r'^posts/(?P<post_title>\w+)$', 'post'),
    url(r'^posts/new$', 'post_new'),
    url(r'^posts/(?P<post_title>\w+)/edit$', 'post_edit'),
    url(r'^posts/(?P<post_title>\w+)/delete$', 'post_delete'),
)
